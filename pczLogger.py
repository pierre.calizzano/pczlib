#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    pcz LIBrary for logging
    Version 2.0.4, 22/jan/24
"""
import os
import logging
import logging.handlers
import traceback

class PczLogger:
    """For Logging"""

    def __init__(
        self,
        logLevel="debug",
        logName="FIXME",
        logFolder="./",
        logFileName="",
        backupCount=3,
        logMaxBytes=10240,
    ):
        self.folder = logFolder
        self.name = logName
        self.logFileName = logFileName
        if self.logFileName == "":
            self.logPath = os.path.join(self.folder, self.name + ".log")
        else:
            self.logPath = self.logFileName = os.path.join(
                self.folder, self.logFileName
            )
        self.level = self.__getLogLevel(logLevel)
        self.backupCount = backupCount
        self.logMaxBytes = logMaxBytes

        # Creation of the Logger
        log = logging.getLogger(self.name)
        log.setLevel(self.level)
        logFileHandler = logging.handlers.RotatingFileHandler(
            self.logPath, maxBytes=self.logMaxBytes, backupCount=self.backupCount
        )
        logFileHandler.setLevel(self.level)
        logConsoleHandler = logging.StreamHandler()
        logConsoleHandler.setLevel(self.level)
        # create formatter and add it to the handlers
        logFormatterSimple = logging.Formatter("%(levelname)s - %(message)s")
        logFormatterAdvanced = logging.Formatter(
            "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        )
        logFileHandler.setFormatter(logFormatterAdvanced)
        logConsoleHandler.setFormatter(logFormatterSimple)
        log.addHandler(logFileHandler)
        log.addHandler(logConsoleHandler)
        self._logger = log

    def __getLogLevel(self, a_sLevel):
        if a_sLevel.lower() == "error":
            return logging.ERROR
        elif a_sLevel.lower() == "info":
            return logging.INFO
        else:
            return logging.DEBUG

    # see https://stackoverflow.com/questions/39492471/how-to-extend-the-logging-logger-class
    def debug(self, msg, extra=None):
        if self.level == logging.DEBUG:
            self._logger.debug(msg, extra=extra)

    def info(self, msg, extra=None):
        if self.level == logging.INFO or self.level == logging.DEBUG:
            self._logger.info(msg, extra=extra)

    def error(self, msg, extra=None):
        self._logger.error(msg, extra=extra)

    def warn(self, msg, extra=None):
        self._logger.warn(msg, extra=extra)

    def exception(self, msg, ex=None):
        if ex is not None:
            tb_lines = traceback.format_exception(ex.__class__, ex, ex.__traceback__)
            tb_text = ''.join(tb_lines)
            msg = msg + '\n' + tb_text
        self._logger.error(msg, exc_info=True)


"""
    def __getLogNameForAction(self, a_action):
        sFileName = "{0}_{2:03d}_{1}.log".format(
            self.ts.strftime(r"%Y%m%d_%H%M%S"), a_action.typeName, a_action.numAction
        ).replace(" ", "_")
        return sFileName

"""


if __name__ == "__main__":
    """
    For testing this package
    """
    # logger = PczLogger("info", "pczlib", "./tests/log/")
    logger = PczLogger(
        logLevel="debug",
        logFolder="./tests/log/",
        logName="pczlib",
        logFileName="myName4Log",
        backupCount=2,
        logMaxBytes=5120,
    )
    logger.error("logging Error from pczLogger")
    logger.info("logging Info from pczLogger")
    logger.debug("logging Debug from pczLogger")
    logger.debug(
        "logging from pczLogger - BIG LINE 1 ************************ for rotation ************************"
    )
    logger.debug(
        "logging from pczLogger - BIG LINE 2 **************************************************************"
    )
    logger.debug(
        "logging from pczLogger - BIG LINE 3 **************************************************************"
    )
    logger.debug(
        "logging from pczLogger - BIG LINE 4 **************************************************************"
    )
    logger.debug(
        "logging from pczLogger - BIG LINE 5 **************************************************************"
    )
    try:
        impossible = 5/0
    except Exception as ex:
        logger.exception("Exception de test", ex)
    finally:
        logger.info("finally : End tests")

