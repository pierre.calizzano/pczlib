#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    pcz Library for sending Emails
    Configuration (smtp connexion string, folders ...) stored in an XML configuration file
    store Emails as an XML file in config.folders.toSend
    When email sended, move the XML file in ./sended or ./error
    Reference: https://realpython.com/python-send-email/
    Version 2.0.4, 08/juin/23
"""
import sys
import traceback
import os

sys.path.append(os.__file__)  # Add current folder to LIB path
import datetime
import smtplib
import ssl
import base64
import email
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import os.path
import xml.etree.ElementTree as ET
from enum import Enum
import pczMiscLib as myLib


LOCK_FILE = "./pczMailer.lock"
CONFIG_PATH = "./pczMailer.config.xml"


class TransportProtocol(Enum):
    DEBUG = 0
    TEXT = 1
    SSL = 2
    TLS = 3


class PczMailer:
    def __init__(self):
        """Constructor
        initialize the global configuration
        """
        self.host = None
        self.port = None
        self.transportProtocol = TransportProtocol.TEXT
        self.login = None
        self.password = None
        self.folderToSend = None
        self.folderSended = None
        self.folderError = None
        self._smtpCnx = None
        self.pathLockFile = None

    def initFromXml(self, configPath):
        """parse config.xml file
        return (False, message) on error
        TODO: control mandatories parameters
        """
        try:
            xDocConfig = ET.parse(configPath)
            xRoot = xDocConfig.getroot()
            if xRoot.find("./config/smtp") is None:
                raise myLib.PczException(
                    "Tag config/smtp is not defined in .config.xml file"
                )

            if xRoot.find("./config/smtp").get("host") is not None:
                self.host = xRoot.find("./config/smtp").get("host")
            else:
                raise myLib.PczException(
                    "Attribut smtp/host is not defined in .config.xml file"
                )
            if xRoot.find("./config/smtp").get("port") is not None:
                self.port = xRoot.find("./config/smtp").get("port")
            else:
                raise myLib.PczException(
                    "Attribut 'smtp/port' is not defined in .config.xml file"
                )
            if xRoot.find("./config/smtp").get("protocol") is not None:
                sProtocol = xRoot.find("./config/smtp").get("protocol")
            else:
                raise myLib.PczException(
                    "Attribut 'smtp/protocole' is not defined in .config.xml file"
                )
            self.transportProtocol = myLib.convertToEnum(sProtocol, TransportProtocol)
            if xRoot.find("./config/smtp").get("login") is not None:
                self.login = xRoot.find("./config/smtp").get("login")
            else:
                raise myLib.PczException(
                    "Attribut 'smtp/login' is not defined in .config.xml file"
                )
            if xRoot.find("./config/smtp").get("password") is not None:
                self.password = xRoot.find("./config/smtp").get("password")
            else:
                raise myLib.PczException(
                    "Attribut 'smtp/password' is not defined in .config.xml file"
                )
            xFolders = xRoot.find("./config/folders")
            if xFolders.find("tosend").text is not None:
                self.folderToSend = xFolders.find("tosend").text
            else:
                raise myLib.PczException(
                    "Tag 'folders/tosend' is not defined in .config.xml file"
                )
            if xFolders.find("sended").text is not None:
                self.folderSended = xFolders.find("sended").text
            else:
                raise myLib.PczException(
                    "Tag 'folders/sended' is not defined in .config.xml file"
                )
            if xFolders.find("error").text is not None:
                self.folderError = xFolders.find("error").text
            else:
                raise myLib.PczException(
                    "Tag 'folders/error' is not defined in .config.xml file"
                )

            if xRoot.find("./config/path_lock_file").text is not None:
                self.pathLockFile = xRoot.find("./config/path_lock_file").text
            else:
                self.pathLockFile = LOCK_FILE

            return (True, "")
        except Exception as e:
            return (False, e)

    def __getBase64(self, a_path):
        """return the BASE64 string of the file content"""
        with open(a_path, "rb") as f:
            b64 = base64.b64encode(f.read())
        return str(b64, "utf-8")

    def addMail(
        self,
        sender,
        to,
        cc=None,
        bcc=None,
        subject=None,
        message=None,
        format="text",
        attachments=None,
    ):
        """Create an XML File
        return( T/F, message)
        """
        try:
            xRoot = ET.Element("root")
            ET.SubElement(xRoot, "from").text = sender
            ET.SubElement(xRoot, "to").text = to
            ET.SubElement(xRoot, "format").text = format
            if cc is not None:
                ET.SubElement(xRoot, "cc").text = cc
            if bcc is not None:
                ET.SubElement(xRoot, "bcc").text = bcc
            if subject is not None:
                ET.SubElement(xRoot, "subject").text = subject
            if message is not None:
                ET.SubElement(xRoot, "message").text = message
            if attachments is not None:
                xAttachs = ET.SubElement(xRoot, "attachments")
                arAttachs = attachments.split(",")
                for sPath in arAttachs:
                    xAttach = ET.SubElement(xAttachs, "attachment")
                    ET.SubElement(
                        xAttach, "path"
                    ).text = sPath  # os.path.basename(sPath)
                    ET.SubElement(xAttach, "base64").text = self.__getBase64(sPath)
            # wrap it in an ElementTree instance, and save as XML
            xTree = ET.ElementTree(xRoot)
            sFileName = "{0}_pczMailer.xml".format(
                datetime.datetime.now().strftime(r"%Y%m%d-%H%M%S_%f")
            )
            sPath = str(os.path.join(self.folderToSend, sFileName))
            sFileContent = ET.tostring(xTree.getroot(), encoding="unicode")
            if format.lower() == "html":
                # Hack CDATA issue (https://stackoverflow.com/questions/174890/how-to-output-cdata-using-elementtree)
                sFileContent = sFileContent.replace("&amp;", "&")
                sFileContent = sFileContent.replace("&lt;", "<")
                sFileContent = sFileContent.replace("&gt;", ">")
            # xml.etree.ElementTree.tostring(xTree, encoding='unicode', method='xml')
            with open(sPath, "w") as fXml:
                # xTree.write(fXml, encoding="unicode", method='xml')
                fXml.write(sFileContent)

            return (True, "")

        except Exception as e:
            print(e)
            print(traceback.format_exc())  # FIXME
            return (False, e)

    def sendMailsNG(self):
        """Send an Email for all the XML file in the <tosend> folder.
        Move the XML file in the <sended> or <error> folder
        return( T/F, NbrEmailsSended, message)
        """
        try:
            if os.path.isfile(self.pathLockFile):
                print("Script is already running => exit")
                return (False, 0, "Script is already running")
            else:
                with open(self.pathLockFile, "w") as lockFile:
                    lockFile.write("Script is running")
            iNbrSended = 0
            bError = not self.__openSmtpCnx()
            if not bError:
                # iterate all XML files to send
                for de in myLib.scanDirRecurse(self.folderToSend):
                    if de.is_file() == False:
                        continue
                    if self.__sendXmlMail(de.path):
                        # OK => move to /sended/
                        sTarget = self.folderSended
                        iNbrSended = iNbrSended + 1
                    else:
                        # KO => move to /error/
                        sTarget = self.folderError
                        bError = True
                    myLib.move(de.path, sTarget)
            os.remove(self.pathLockFile)
            return (not bError, iNbrSended, "")
        except Exception as e:
            os.remove(self.pathLockFile)
            print("Exception in sendMails() \n{0}".format(e))
            return (False, iNbrSended, e)
        finally:
            self.__closeSmtpCnx()

    def sendMails(self):
        """Send an Email for all the XML file in the <tosend> folder.
        First version with return( T/F, message)
        For compatibility only
        """
        (bResult, iNbrSended, sError) = self.sendMailsNG()
        return (bResult, sError)

    def __openSmtpCnx(self):
        """start connexion to the SMTP server

        return FALSE if error occured
        """
        try:
            if self.transportProtocol == TransportProtocol.SSL:
                sslContext = ssl.create_default_context()
                self._smtpCnx = smtplib.SMTP_SSL(
                    host=self.host, port=self.port, context=sslContext
                )
                self._smtpCnx.login(self.login, self.password)
            elif self.transportProtocol == TransportProtocol.TLS:
                sslContext = ssl.create_default_context()
                self._smtpCnx = smtplib.SMTP(host=self.host, port=self.port)
                self._smtpCnx.ehlo()  # Can be omitted
                self._smtpCnx.starttls(context=sslContext)  # Secure the connection
                self._smtpCnx.ehlo()  # Can be omitted
                self._smtpCnx.login(self.login, self.password)
            return True
        except Exception as e:
            print("Exception in __openSmtpCnx() \n{0}".format(e))
            return False
        # EndOf __openSmtpCnx()

    def __closeSmtpCnx(self):
        """close the connexion"""
        if self._smtpCnx is not None:
            self._smtpCnx.quit()

    def __sendXmlMail(self, a_xmlPath):
        """read information in the XML file and send then mail
        return True/False
        """
        try:
            xDoc = ET.parse(a_xmlPath)
            xRoot = xDoc.getroot()
            sFormat = xRoot.find("format").text
            bText = True if sFormat.lower() == "text" else False
            sSender = xRoot.find("from").text
            sReceivers = xRoot.find("to").text
            sSubject = xRoot.find("subject").text
            sBody = xRoot.find("message").text
            # "Subject: Hi there\n\n\nThis is a test from PYTHON"
            message = MIMEMultipart()
            message["From"] = sSender
            message["To"] = sReceivers
            message["Subject"] = sSubject
            if bText:
                message.attach(MIMEText(sBody, "plain"))
            else:
                # multipart is not rquired with  Thunderbird (and with other client?)
                # soup = BeautifulSoup(sBody, "html.parser" ) #lxml : not find on DSM :()
                # part1 = MIMEText(soup.get_text(" "), "plain")
                part2 = MIMEText(sBody, "html")
                # message.attach(part1)
                message.attach(part2)
            for xAttach in xRoot.findall("./attachments/attachment"):
                sPath = xAttach.find("path").text
                sBase64 = xAttach.find("base64").text
                if sBase64 is None:
                    sBase64 = self.__getBase64(sPath)
                sPath = os.path.basename(sPath)
                contentDecoded = base64.b64decode(sBase64)
                part = MIMEBase("application", "octet-stream")
                part.set_payload(contentDecoded)
                email.encoders.encode_base64(part)
                part.add_header(
                    "Content-Disposition",
                    f"attachment; filename= {sPath}",
                )
                message.attach(part)
            sMessage = message.as_string()
            self._smtpCnx.sendmail(sSender, sReceivers, sMessage)
            return True
        except Exception as e:
            print("Exception in __sendXmlMail() \n{0}".format(e))
            return False

    # EndOf class PczMailer


if __name__ == "__main__":
    try:

        print("Create PczMailer() class,")
        myMailer = PczMailer()
        print("Initialize from XML configuration file,")
        myMailer.initFromXml(CONFIG_PATH)

        print("Add mail in <tosend> folder")
        ts = datetime.datetime.now()
        (bResult, sError) = myMailer.addMail(
            sender="pczlib@exemple.fr",
            to="test@exemple.fr",
            subject="Test pczMailer.addMail(TEXT)",
            message="blabla from pyMailer.py"
            + "\n TimeStamp= {0}".format(ts)
            + "\nEOF",
            format="text",
        )
        if not bResult:
            print(f"Error on adding email #1 : '{sError}' ")

        (bResult, sError) = myMailer.addMail(
            sender="pczlib@exemple.fr",
            to="test@exemple.fr",
            subject="Test pczMailer.addMail(HTML)",
            message="<![CDATA[ \n <p>blabla from pyMailer.py</p>\n"
            + "<p> TimeStamp= {0} </p>".format(ts)
            + '<p style="color:red">paragraph in red (on error)</p>'
            + "\n<p>EOF</p>]]>",
            format="html",
        )
        if not bResult:
            print(f"Error on adding email : '{sError}' ")

        print("Sending mails in <tosend> folder")
        # Obsolete version : myMailer.sendMails()
        (bResult, iNbrSended, sError) = myMailer.sendMailsNG()
        if not bResult:
            print(f"Error on sending email : '{sError}' ")
        else:
            print(f"{iNbrSended} Mail(s) sended.")

    except myLib.PczException as e:
        print("PczException, Fatale error : " + e.message)
    except Exception as e:
        print("EXCEPTION, Fatale error : " + e)
    finally:
        print("Ending tests")
