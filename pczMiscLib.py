# -*- coding: utf-8 -*-

"""
    Various function and Class
    Version 2.0.5, 26/juin/24 (runCmd withShell)
"""
import os
import os.path
import platform
import shutil
import ntpath
import subprocess as sp
import time
import requests

# import datetime
from threading import Timer
import shlex


def getTimeStamp():
    """return the string "AAMMJJ-HHMMSS" """
    ts = time.gmtime()
    return time.strftime("%Y%m%d-%H%M%S", ts)


def move(a_sourcePath, a_targetFolder):
    """move a file to a destination folder
    return T/F
    """
    try:
        sFileName = ntpath.basename(a_sourcePath)
        sTargetPath = os.path.join(a_targetFolder, sFileName)
        shutil.move(a_sourcePath, sTargetPath)
        return True
    except:
        return False


def scanDirRecurse(a_rootFolder):
    """Usage:
    for de in scanDirRecurse( a_rootFolder ):
        if de.is_file() == False :
            continue
    """
    for de in os.scandir(a_rootFolder):
        if de.is_dir():
            yield from scanDirRecurse(de.path)
        else:
            yield de


def sendNtfy(title, message, xRootConfig):
    """
    Send a notification to https://ntfy.sh/
    return TRUE if notification sended
    """
    if xRootConfig.find("./config/ntfy") is not None:
        sNtfyID = xRootConfig.find("./config/ntfy").text
        sTags = (
            xRootConfig.find("./config/ntfy").get("Tags")
            if xRootConfig.find("./config/ntfy").get("Tags") is not None
            else platform.node()
        )
        sPriority = (
            xRootConfig.find("./config/ntfy").get("Priority")
            if xRootConfig.find("./config/ntfy").get("Priority") is not None
            else "urgent"
        )
        requests.post(
            "https://ntfy.sh/" + sNtfyID,
            data=message,
            headers={"Title": title, "Priority": sPriority, "Tags": sTags},
        )
        return True
    else:
        return False


def runCmd(a_cmd, a_timeout, a_withShell = False):
    """start a subprocess, wait for returnCode
    return (returnCode, logMessage)"""
    rc = -1
    sMsg = "Timeout Exception"
    if myIsDigit(a_timeout):
        iTimeout = int(a_timeout)
    else:
        iTimeout = None
    if a_withShell:
        proc = sp.Popen(shlex.split(a_cmd), shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    else:
        proc = sp.Popen(shlex.split(a_cmd), stdout=sp.PIPE, stderr=sp.PIPE)
    timer = Timer(iTimeout, proc.kill)
    try:
        timer.start()
        stdout, stderr = proc.communicate()
        rc = proc.returncode
        if rc == 0:
            sMsg = str(stdout)
        else:
            sMsg = str(stderr)
    finally:
        timer.cancel()
    return (rc, sMsg)
    # EndOf runCmd()


def createSshRemotePath(a_host, a_user, a_path):
    """Create a remote path : admin@server.local://path/to/sync/
    TODO: check/control all argument are valid
    """
    sCmd = "{0}@{1}:".format(a_user, a_host)
    if a_path[:2] == r"//":
        pass
    elif a_path[:1] == r"/":
        sCmd += r"/"
    else:
        sCmd += r"//"
    sCmd += a_path
    return sCmd
    # EndOf createSshRemotePath()


def convertString2Bool(a_str):
    """Convert a string to a boolean

    ignore case
    Yes, y, Oui, O, true, T => True
    no, non, n, false, F    => False
    """
    if a_str is None:
        return False
    myStr = a_str.lower()
    if (
        myStr == "yes"
        or myStr == "y"
        or myStr == "oui"
        or myStr == "o"
        or myStr == "true"
        or myStr == "t"
    ):
        return True
    else:
        return False
    # EndOf convertString2Bool()


def convertString2Int(a_str, a_default=None):
    """Convert a string to an integer
    return Default if not an integer
    """
    try:
        return int(a_str)
    except Exception:
        return a_default
    # EndOf convertString2Int()


def convertToEnum(a_var, a_enum, default=None):
    """If var is an Enum member, return this member else return None"""
    if a_var is None:
        return default
    for m in a_enum:
        if m.name.upper() == a_var.upper():
            return m
    return default
    # EndOf convertToEnum()


def myIsDigit(a_var):
    """Works for any type of variable (sould do)
    return T/F"""
    if isinstance(a_var, str):
        return a_var.isdigit()
    else:
        return False
    # EndOf myIsDigit()


class PczException(Exception):
    """managed exception"""

    def __init__(self, message):
        # self.when = datetime.now()
        self.message = message
        super().__init__(self)

    def __str__(self):
        s = self.message
        return s

    # EndOf PczException()


# Usage and tests
if __name__ == "__main__":
    try:
        print("Starting tests")

        print("getTimeStamp() => " + getTimeStamp())


        sPath = "/tmp/"
        print(f"start scanDirRecurse( {sPath} )")
        for de in scanDirRecurse(sPath):
            if de.is_file() == False:
                continue
            else:
                print(f"file: {de}")

        sInput = input("Enter a boolean string :")
        print(f"{sInput} is {convertString2Bool(sInput)}")

        sInput = input("Enter an integer string :")
        print(f"{sInput} is {convertString2Int(sInput)}")

        sInput = input("Enter an digit string :")
        print(f"{sInput} is a digit : {myIsDigit(sInput)}")

    except PczException as e:
        print("PczException, Fatale error : " + e.message)
    except Exception as e:
        print("EXCEPTION, Fatale error : " + e)
    finally:
        print("Ending tests")
