#! /usr/bin/python3
# -*- coding: utf-8 -*-

"""
    send Emails as XML with the PczMailer librairie
    Version 2.0.3, 23/jan/23
    Version 2.0.4, 15/jan/24
    Version 2.0.5, 15/jan/24
"""
import os
import argparse
import pczMailer
import pczMiscLib as myLib

CONFIG_PATH = "./pczmailer.config.xml"

try:
    rc = 0
    os.chdir(os.path.split(__file__)[0]) #for cron
    argParser = argparse.ArgumentParser(
        description="send emails created with the pczMailer library"
    )
    argParser.add_argument(
        "configPath",
        nargs="?",
        default=CONFIG_PATH,
        help="Path for the configuration file",
    )
    args = argParser.parse_args()
    if args.configPath:
        if not os.path.isfile(args.configPath):
            print("argument #1 (Configuration file) is an invalide path")
            exit(1)

    print("Create PczMailer() class,")
    myMailer = pczMailer.PczMailer()
    print(f"Initialize from XML configuration file ({args.configPath}),")
    myMailer.initFromXml(args.configPath)
    print("Send mail(s) in folder ...")
    (bResult, iNbrSended, sError) = myMailer.sendMailsNG()
    if not bResult:
        print(f"Error on sending email : '{sError}' ")
        rc = 1
    else:
        print(f"{iNbrSended} Mail(s) sended.")
        rc = 0
except myLib.PczException as e:
    print("PczException, Fatale error : " + str(e) )
    rc = -1
except Exception as e:
    print("EXCEPTION, Fatale error : " + str(e) )
    rc = -2
finally:
    print("Ending with Return code " + str(rc))
    exit(rc)
