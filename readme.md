# README of pczLib

## Usage
Python package I used for my projects.

### Logging
```
import pczMiscLib as myLib
import pczLogger

logger = pczLogger.PczLogger(logLevel="info", logFolder="./log/", logName="MyApp" )
logger.error("logging Error from pczLogger")
logger.info("logging Info from pczLogger")
logger.debug("logging Debug from pczLogger")

Named parameters : 
 logLevel=, 
 logFolder=, 
 logName=, 
 logFileName=, 
 backupCount=, 
 logMaxBytes=
```

### Mailing
```
import pczMailer
arEmailBody = [" "]
myMailer = pczMailer.PczMailer()
myMailer.initFromXml(g_xRoot.find("./mailer").get("configPath"))
arEmailBody.append( f" '{SCRIPTNAME}' report,  run on {sStartingDate}, at {sStartingTime}, on {sHostName}" )
finally:
        arEmailBody.append(f"Ending script with ReturnCode= {rc}")
        sMessage = "\n".join(arEmailBody)
        if rc == 0:
            myMailer.addMail(
                sender=g_xRoot.find("./mailer").get("from"),
                to=g_xRoot.find("./mailer").get("to"),
                subject=f"Script {SCRIPTNAME}, Execution report.",
                message=sMessage,
                attachments=sPathToSave )
        else:
            myMailer.addMail(
                sender=g_xRoot.find("./mailer").get("from"),
                to=g_xRoot.find("./mailer").get("to"),
                subject=f"Script {SCRIPTNAME}, Error report.",
                message=sMessage )
        return rc
```
Use ``pczSendMails.py`` in ``crontab`` for sending emails created by ``myMailer.addMail()``

## Installation
install python >3.6.4 : goto https://www.python.org/downloads/

install dependancy : ``pip install beautifulsoup4``

Copy in the same folder as your python project or in the $PYTHONPATH folder.

## import in your script
```
import pczMiscLib as myLib
import pczMailer
import pczLogger
```
When sending mails, **if you have this error** "``[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate``", 
run '``pip install --upgrade certifi``' 

(from https://stackoverflow.com/questions/50236117/scraping-ssl-certificate-verify-failed-error-for-http-en-wikipedia-org) 

## Configuration file (template)
```
# pczlib.config.xml
<root>
  <logging level="debug|info|error" folder="./log/" name="pczlib" maxbytes="10240" backupcount="3"/>
  <mailer configPath="./pczMailer.config.xml" from="pczlib@exemple.fr" to="test@exemple.fr" />
</root>

# pczmailer.config.xml
<root>
    <config>
	    <smtp host="SSL.EXEMPLE.NET" port="465" protocol="SSL" login="test@exemple.fr" password="PASSWORD" />
	    <folders>
		    <tosend>/home/you/develop/pczlib/tests/pczMailer/tosend</tosend>
		    <sended>/home/you/develop/pczlib/tests/pczMailer/sended</sended>
		    <error>/home/you/develop/pczlib/tests/pczMailer/error</error>
	    </folders>
	    <path_lock_file>/home/you/develop/pczlib/tests/pczMailer/pczMailer.lock</path_lock_file>
    </config>
</root>
```
